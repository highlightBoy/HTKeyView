//
//  ViewController.m
//  HTKeyViewExmple
//
//  Created by  易万军 on 15/7/24.
//  Copyright (c) 2015年 yiwanjun. All rights reserved.
//

#import "ViewController.h"
#import "HTKeyView.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSArray *keys = @[@"四核",@"看得开",@"等我",@"啊是斯",@"放歌是",@"啊飒飒的啊飒飒的",@"阿斯达斯",@"阿",@"中丝袜啊",@"阿啊",@"阿有啊",@"思科打开",@"思科打开",@"思科打开",@"粉色的的",@"说"];
    
    HTKeyView *htView = [[HTKeyView alloc]initWithFrame:CGRectMake(8, 120, CGRectGetWidth(self.view.frame)-16, 120)];
    [htView setBackgroundColor:[UIColor lightGrayColor]];
    [self.view addSubview:htView];
    [htView loadKeys:keys];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
